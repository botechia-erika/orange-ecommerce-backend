import { BaseDatabase } from "./BaseDatabase";
import { TProductDB, TUserDB, USER_ACCOUNT } from "../types/types";
import { ProductsPurchases } from "../models/ProductsPurchases";
import { IPurchaseDB } from "../interfaces/interfaces";
import { Purchases } from "../models/Purchases";
// CRIACAO DO BASEDATABASE  serve para EXTRAIR A LOGICA ASSOCIADA A EXTRACAO DE INFO DO BANCO DE DADOS, A PARTE QUE FAZ A REQUISICAO DA INFO NAO PRECISA SABER COMO A LOGICA E EXECUTADA

export class PurchasesDataBase extends BaseDatabase {
  public static TABLE_PURCHASES = "purchases";
  public async findPurchases(q: string | undefined) {
    let purchaseDB;
    if (!q) {
      const result: IPurchaseDB[] = await BaseDatabase.connection(
        PurchasesDataBase.TABLE_PURCHASES
      );
      purchaseDB = result;
      return purchaseDB;
    } else {
      const result: IPurchaseDB[] = await BaseDatabase.connection(
        PurchasesDataBase.TABLE_PURCHASES
      ).where("buyer", "LIKE", `${q}`);

      purchaseDB = result;
      return purchaseDB;
    }
    return purchaseDB;
  }

  public async findPurchaseById(id: string) {
    const result: IPurchaseDB[] = await BaseDatabase.connection(
      PurchasesDataBase.TABLE_PURCHASES
    ).where("id", "like", `${id}`);

    const productDB = result;
    return productDB;
  }

  public async insertProductPurchase(
    productsPurchase4Insert: IPurchaseDB
  ) {
   await BaseDatabase.connection(
      PurchasesDataBase.TABLE_PURCHASES
    ).insert(productsPurchase4Insert);
  }

  public async updatePurchase(purchases4Edit: Purchases) {
    const result: IPurchaseDB[] = await BaseDatabase.connection(
      PurchasesDataBase.TABLE_PURCHASES
    ).where("id", "LIKE", `${purchases4Edit.getId()}`);
  }

  public async deletePurchase(id4Delete: Purchases) {
    await BaseDatabase.connection(
      PurchasesDataBase.TABLE_PURCHASES
    )
      .delete()
      .where("id", "LIKE", `${id4Delete.getId()}`);
  }
}
