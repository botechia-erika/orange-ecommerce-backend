
CREATE TABLE USERS (
    ID TEXT PRIMARY KEY NOT NULL UNIQUE,
    NAME TEXT NOT NULL,
    NICKNAME TEXT NOT NULL UNIQUE,
    EMAIL TEXT NOT NULL UNIQUE,
    PASSWORD TEXT NOT NULL,
    CREATED_AT DATETIME DEFAULT CURRENT_TIMESTAMP
);

DROP TABLE USERS;

INSERT INTO USERS(
    ID,
    NAME,
    NICKNAME,
    EMAIL,
    PASSWORD
) VALUES (
    "u001",
    "MARCELO REZENDE",
    "MARCELO-REZENDE",
    "marcelo@email.com",
    "marcelo123"
),
(
    "u002",
    "LAURA SILVA",
    "lau-silva",
    "lau@email.com",
    "lau123"
);

SELECT
    *
FROM
    USERS
WHERE
    NAME LIKE '%BELTRANO%';

SELECT
    *
FROM
    USERS
WHERE
    NAME LIKE '%ERIKA%';

SELECT
    *
FROM
    USERS
WHERE
    NAME LIKE '%FULANO%';

SELECT
    *
FROM
    USERS;

SELECT
    ID,
    CREATED_AT
FROM
    USERS
ORDER BY
    ID DESC;

CREATE TABLE PRODUCTS(
    ID TEXT PRIMARY KEY NOT NULL UNIQUE,
    NAME TEXT NOT NULL,
    DESCRIPTION TEXT NOT NULL,
    IMAGE_URL TEXT NOT NULL,
    PRICE REAL NOT NULL
);

SELECT
    *
FROM
    ACCOUNTS;

INSERT INTO ACCOUNTS (
    ID,
    OWNER,
    BALANCE
) VALUES (
    "a001",
    "f001",
    100
);

INSERT INTO PRODUCTS(
    ID,
    NAME,
    DESCRIPTION,
    IMAGE_URL,
    PRICE
) VALUES (
    "P001",
    "PREMIUM-MUSIC",
    "MENSAL",
    "https://cms-fym.imgix.net/free_apple_music_362cfbaf74.png?auto=compress,format&fit=fillmax&ch=Save-Data&w=1600&max-h=1600",
    7.00
),
(
    "P002",
    "PREMIUM-PLANNER",
    "MENSAL",
    "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxEQEBAPDw8VDxEQGBIQEA8QEA8NEBAQFRIYFhYSFhUYHCgsGCYlHRMTITEhMSo3Li8vFyA4ODMsOCguLisBCgoKDg0OGxAQGy0lICUuLi0wLzA2Ny0vLS8vLi8rLS0tKy8tLS0vLS4tLS0vLS4tLS0rNy0rLS0tLS0tLS0tLv/AABEIAJ8BPgMBIgACEQEDEQH/xAAbAAEAAgMBAQAAAAAAAAAAAAAABQYBBAcDAv/EAEYQAAEDAgIDCwYNBAIDAQAAAAEAAgMEEQUSITGTBgcTIkFRVGFx0dIUFjKBkbEVFyMlNVVyg6Gys7TBQlJi8JKiM8LhNP/EABsBAQACAwEBAAAAAAAAAAAAAAAFBgECAwQH/8QAOhEAAgECAgQJCgYDAAAAAAAAAAECAxEEMSFBUZEFEhMiYXGxwfAGMjRTgYKSodHhFBUzUlTCcqLi/9oADAMBAAIRAxEAPwDsSIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCItSurRG11tLgCbcxA5VyrV6dGPHqOy8aFtfUbRi5OyK5iO6+R1RLSYbSeWyQHLUTPkFPTQv/ALC86XHqC0MZ3SYxS08tTJQ0ZZC3O8MqZ3PyjXYFgv7V871DfmuKQjjzSVMsp5XSGoe3MefQxo9StD3xyB0ZLJA4Fj2EteCCLFrm/wALtLQ7Gi0q5yH496joEO1lX2N/KqOkYfEfvJVfHbgsL+r4vY8fyvnzBwroEX/fvWLgoh39akWvh8QvpHykukKQ3G7tsTr53zQQxv4sokjmqZWQCz4izKxrSGFoc4A2u7M7MTlFrBiO5LB4GgvoI3OeckUTA90sz7XyMbm0nlJ1AAkkAEqU3NYIykjdlijifKQXRwi0cTRfJE063ZbuJcdbnOOgEALmTVqN2dZR2kxLD2tp7gPqaOY1DYbm13xuANutXWCZsjGyMcHseA9jhpDmkXBHqKha+BssUsTxmZIx7HNOkFrmkEfioLelxInCaRsmnKHsDtZDRI4Aexcq+IpUYceq7K6V9Wm+ezLPIzGLk7RL2iw0gi4NweULK6mAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCItSvqMoyjWdZ5lwxOJhh6TqzyXzepLpf3yNoQc5cVHxWVn9LD2u/gKKqTxHn/F35SvZVfdBjRcTTU/GLvk3ubxtJ4vBstr5lSVLEcJ4pX1boxv46W/lM4fDOXMhv72V3c3jT4cKpqRgLH/LGVx4rg188jg1va1wN+v2at+1SuLYK+nbdxzWysmLRxIpnAu4HNfjENsTbQLqKV8cuM2yawVGlRpKNLLbtfjLotbaS+HB/BvcJqhjm3DWwwVckZsM3GlYMrfbo5VP7nsRrKmBtgyHKXMdNOeGlNrEWiYbHiu9Iv0aOKdKprJ3NDmteWtPpNDiGu7QNat+4aX5OVnM5rv8Ak23/AKLBHcKUJ8jOpJp6U1oSaWTV82TVDhzIiXlzpZnAB88pDpHD+0WADBovlaA2+m11uErBK+SVkrZiU6D2H3Kob1P0TTfe/quVukOg9h9yqO9R9E033v6rlDeUHoXvx7JHowv6vsfcXSmqSw845R/IUtG8OAINwVBr3o6jIdPonX1KE4H4UeHkqNV8x/6/bbszyuemvR4y4yz7SWREV1I4IiIAiLWmxGBjssk8bHDW18rGuHqJQyouTslc2UWn8LUvSodvF3p8LU3SodvF3obclP8Aa9xuItP4WpulQ7eLvT4WpulQ7eLvS45Kp+17jcRafwvS9Kh28XenwvS9Kh28Xeg5Kex7jcRafwvS9Kh28XenwvS9Kh28Xeg5Kex7jcREQ0CIiAIiIAiIgCIiAIiIDEjw0Fx1DSoR7y4knWdC38Tk0Bv92v1f7+Cou6PHvSggPVJIPxa0+8qr8KqrjcXHC0so6W9Sb1vqTVutoleD8NKplr+SPvH8aLj5NTXe5xyOcy5NzxcjMus8l174NgPBskjnjLKlwLiwsZK80obd3kwuQZCQBc6W83P54RRCmhjrmOY5wBkfI9w4FjQLeSgAFxkJI020W0X03+qqtNTwcGHQNORgewhr2T0b83HHDZ7a9N+XNyqbwmEp4alydNdb1t7X40EnJ6OTou0dcstKvnfQle2h5q9tSlnF8Yma0xOYJZ4mlrqgF85poX6HMeLZeEADWl/Xa91GT7l3ktdA9s1PI0yCoJyRsa0cYSn+gjVb/wC2n8DpvJnPLZntp5gGTSO4OOalqY9OSW9wB6YudBzjXoJrGKY0aguZDEKeN7szooS8Nkfm4pc3U46tQ0nsFvWb4dycuLRSS1vr6M0078VJtZtq0lJxDW3sALk6ABpJPMr5uXwp1OxzpPTly3H9oF7A9fGN147nMB4ECSQXlPot1iMd/WrAsojuE+EVVvRpebre3q6OnX1Z/LnAAkmwGkk6ABzoSoLdhK4QFjQflTlceQMGkj16B6yoGgxiozwRyS5IozGHOAIL2t/vd2C3vXSKg15yT+2jeeKlwfVq0uVjlp+S8IvEp0HsPuVQ3pz81QdRkH/YH+VaGztewuY4PFiLtIcL21aFV96b6Kg+1J+YKE4e9Cf+Ue844ZNVUnsZcURFRSTJPD5rtynW1bSh6OTK9p59amFe+BMU6+FSlnHm+zV8tHWmRuJhxZ9YRFo45SyTU8kUMnBSOtZ9y3U4EtJGkXAI9alzjBJySbstuzpN6yrGJ7io55pJuGe0vOYtytcA467FRHmfXdJZtqjwp5n13SWbao8KwS1GlCg26eJim+g3fi/j6Q/Zt70+L+PpD9m3vWl5n13SWbao8KeZ9d0lm2qPCsHp5ap/LW43fi/j6Q/Zt70+L+PpD9m3vWl5n13SWbao8KeZ9d0lm2qPCg5ap/LW43fi/j6Q/Zt70+L+PpD9m3vWl5n13SWbao8KeZ9d0lm2qPCg5ap/LW43fi/j6Q/Zt70+L+PpD9m3vWl5n13SWbao8KeZ9d0lm2qPCg5ap/LW4vyIi2K8EREAREQBERAEREAREQEViLryHqt3rmmNYW+B+nS118j+QjmPWF0erPHetSrpWSsMcjczXe0HkIPIVSKXCcsNjas3pjKTutdk2k10pb+yfwVfkEtllfx7SjYLUy8emZIxsdTxH8MWiJvLn0+iRbQdfrsrMx8NHwTcnDUM/BP8p0vPlLDfhCBYttYXj5m6NOYGq4xhb6Z9vSY70H8jhzHmPUtdlVLwboQ9xjc4OdGDxXOGgG3+6hzBXGlUhVipwd08vHcStSiq9pReh56r9N1p4ysrX0aLPMkcZxh873NblGc2e+Bj421JDuI97C45jqIHWpCnp4cNgdWVhsWjitFnFpOgMaP6nHV1e0r63D0sUkLasHPnMjYyRYMDHujJHWS06eZUjfcxbha2KkLiIacMdJl0nPJpc63KRGW2+0eddEiDx+PTjyFB83W9vV0dqsskiJ3R7vqyrc4MkdSw/wBMULixxH+cgsSeyw6lWDM++bO7NrzZnZr897rqPlO5ClsMsta8AXc7ytxOjlDixv4LIx7cjLxX0MkI/uDJo/0ZCVsQ5UMA3c1lKQ10hqodToKhxkuP8Xm5b+I6l1TCRTV0TKmnNo33DmEWfG8ekwjkI9moi4IUA3cBguJA/A+JcHNbMIJXcLoHPG8NkHbcgcyhd72WXD8Unw2ci7y+J4Y4vZw8TS9r2nraHDVfSL6rLlUoxqed48azrSr1KTbg7XOqMiaxhawBrQDoHZrVc3qPomm+9/VcrLIdB7D7lWt6j6Jpvvf1XKJ8oPQvfj2SN8M2613sfcW5ERUgkj5U8x1wDzgfiFBqYpD8m3sVk8m5Wq1I7Unudv7Hkxi5qZ6oiK3HgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiICGqxx3rzWziLLPJ5wO5ay+cY6m6eJqRf7n23XyJem7wT6DXrKVkrDHI3M0+0HkIPIVG0mAxQZ3gl7srspfl4ug6rDX1qaXnUeg/7L/yrWjiasIunGTUXmtTy8PadeUnGLinoZWN6w/NNL21H7mRcw3zIS3FKonRn4J7etvAMbf2tcPUum715+aaXtqP3Miid9bc26eNlZA3NJAC2VrRdz4LkhwHKWkk25nHmX0ufnvrfaQMckem5LHMY8jp24VgMDGBjW+UPysbOWjK6Wznxk5iCb3OvWVLzY5ulaD5VglNVRcrInMuRyi3DPv/AMVxJuL1PBtiFXOIm+hEKiYRNGuzWB1gvqlxiqidmiq543DlZUTM9ztK1Ni77oqzBqiGd7qObBMUgbnjgZG9jZJhpY3KGgNubcYtY4a7myrm9+xz8UpDpJD5JHE3J/8AE8kk9ZP4rz3Q7rqvEIoIqx0cppy4tqOCayocCLZHvGgjqsNNibq7b1e5x0TXV0zcrpW5IGnQRESC6Qj/ACIbbqH+SA6FIdB7D7lXN6j6Jpvvf1XKwynQew+5V7eo+iab739VyhfKD0L349kjvhf1fY+4tyIio5JhTFIPk29ihVPMbYAcwA9isnk3C9WpLYkt7v8A1PJjHzUukyiIrceAIiIAiIgCIiAIiIAiIgCLn3k+Nc79rD408nxrnftYfGsXJL8uXrqfxfY6Ci595PjXO/aw+NPJ8a537WHxpcfly9dT+L7HQUXPvJ8a537WHxp5PjXO/aw+NLj8uXrqfxfY6Ci595PjXO/aw+NPJ8a537WHxpcfly9dT+L7HQUXPvJ8a537WHxrZw6DF+FZwhIZmZnzyRublvxri5Oq+pDD4PSTfLU/i+xeERFkjjTxOO4DubX6/wDfxUcpx7AQQdR0KFlYWktOsKneUOFcKyrpaJaH1r6pK3UyQws7x4uw+V51HoP+y/8AKvRedR6D/sv/ACqAh5y9nael5FU3sD81U3bUfuZFabqq72J+aqbtqP3MitBK+pT859b7SEjkim7ot7ulqXOlicaSV1y7I0Pic48pj0W9RCrfxUz3/wD2RW5+Dkv7L/yuqLBK1MlMwDe6padzZJ3GrkbYgPaGQtI5eDub+skdSuRKXXygEmo9h9yr+9R9E033v6rlOyHQew+5QW9R9E033v6rlC+UHoXvx7JHowv6vsfcW5ERUckz2o48zxzDX/vsUutXD4crcx1u/wBC2le+BcK6GGTlnLnPuW7VtbIzEz40+oIiKXOAREQBERAEREAREQBERAUDzyrujR7Co8aeeVd0aPYVHjV/RYsSf46h6iO9/QoHnlXdGj2FR4088q7o0ewqPGr+iWH46h6iO9/QoHnlXdGj2FR4088q7o0ewqPGr+iWH46h6iO9/QoHnlXdGj2FR4088q7o0ewqfGr+l0sPx1D1Ed7+hB7lsVnqGSOqIhGWkBrmtfG14I0izidWjT1qcRFk8FWcZzcox4q2bAiIhzC1a6nzDMPSH49S2kXDE4eGIpulPJ+E11PSbQk4u6IJedR6D/sv/KpSso73czXyt5+sKKn9B/Y78qoOKwVTCVlCp7HqenV3rVublIVFON0VLex+iqbtn/cyK0XVW3sj81U3bP8AuZFZyV9Iqec+t9pDxyRm6+VhYutTJlLrF1hAfMnonsPuUHvUfRNN97+q5Tcp4ruw+5Qm9R9E033v6rlCeUPoXvx7JHowv6vsfcW5bFFT5zc+iNfWsUtMX9TeU9ylGMAAAFgFDcD8FOvJVqq5iyX7v+e3LK56q9fi81Z9h9IiK5kcEREAREQBERAEREAREQBERAEREAREQBERAEREAREQBERAEREAWpWUoka4anEFoPWRyraK16jNbi61zq0YVY8SorozGTi7o5nvaTDyHyc6JaWWeKZh0Oa4zOeLjsd+B5lalDY3uGdNUOrKeZ9FUP8A/JJA6zZet7NRWt5n4p9bybCHuXZ6W2aJWRYSUVe8z8U+t5NhD3LHmdin1vJsIe5LdJksKwq/5m4p9bybGHuTzNxT63k2MPclukwSuLVrIIJppDlZGxziT2aAOsmwA5ym9RhpbhNGZb8Zrnhuriue4tPssVDje7lmex1fWy1rGEOEDiIoSRylrda6NSMLWtbYNDQGhrRYAAWAAXGtRp1oqNRXSadtV1ffmbRlKLujaaFlYCyugCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAvghfaIDzyrGVelksgPPKmVelksgPPKmVelksgPjKsgL6slkACyiIAiIgCIiAIiIAiIgCIiAIiIAiIgP/9k=",
    7.00
),
(
    "P003",
    "PREMIUM-TASKS",
    "MENSAL",
    "https://cms-fym.imgix.net/how_much_is_spotify_premium_e81b7f47ff.png?auto=compress,format&fit=fillmax&ch=Save-Data&w=1600&max-h=1600",
    8.00
);

SELECT
    *
FROM
    PRODUCTS;

SELECT
    *
FROM
    PRODUCTS_PURCHASES;

DROP TABLE USERS_TASKS;

CREATE TABLE CLASSROOMS(
    ID TEXT PRIMARY KEY UNIQUE NOT NULL,
    NAME TEXT NOT NULL
);

CREATE TABLE STUDENTS (
    ID TEXT PRIMARY KEY UNIQUE NOT NULL,
    NAME TEXT NOT NULL,
    EMAIL TEXT NOT NULL,
    CLASSROOM_ID TEXT NOT NULL,
    FOREIGN KEY (CLASSROOM_ID) REFERENCES CLASSROOMS(ID)
);

-- selecionar buyer todos os pagamentos com id associado e lista de produtos

SELECT
    *
FROM
    PURCHASES
WHERE
    ID = PG001 JOIN PRODUCTS
WHERE
    PRODUCTS.ID=PRODUCT_ID;

-- AULA INTRO SQL
CREATE TABLE IF NOT EXISTS BOOKS(
    ID TEXT PRIMARY KEY UNIQUE NOT NULL,
    NAME TEXT NOT NULL,
    AUTHOR TEXT NOT NULL,
    PAGE_COUNT INTEGER,
    PRICE REAL NOT NULL
);

DROP TABLE BOOKS;

SELECT
    *
FROM
    BOOKS;

INSERT INTO BOOKS(
    ID,
    NAME,
    AUTHOR,
    PAGE_COUNT,
    PRICE
) VALUES(
    "3012928",
    "O Quinze",
    "Raquel de Quiroz",
    208,
    24.9
);

INSERT INTO BOOKS(
    ID,
    NAME,
    AUTHOR,
    PAGE_COUNT,
    PRICE
) VALUES(
    "8503012928",
    "O Quinze",
    "Raquel de Queiroz",
    208,
    24.95
);

INSERT INTO BOOKS(
    ID,
    NAME,
    AUTHOR,
    PRICE
) VALUES(
    "8578897239",
    "Dom Casmurro",
    "Machado de Assis",
    46.77
);

UPDATE BOOKS
SET
    PAGE_COUNT = 463
WHERE
    ID="8503012928"
 -- TASKS
    CREATE TABLE TASKS ( ID TEXT PRIMARY KEY UNIQUE NOT NULL, TITLE TEXT NOT NULL, DESCRIPTION TEXT NOT NULL, CREATED_AT DATETIME DEFAULT CURRENT_TIMESTAMP, STATUS INTEGER NOT NULL DEFAULT 0 );

SELECT
    *
FROM
    TASKS;

DROP TABLE TASKS;

CREATE TABLE USER_TASKS (
    USER_ID TEXT NOT NULL,
    TASK_ID TEXT NOT NULL,
    FOREIGN KEY (USER_ID) REFERENCES USERS(ID) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (TASK_ID) REFERENCES TASKS(ID) ON UPDATE CASCADE ON DELETE CASCADE
);

SELECT
    *
FROM
    USER_TASKS;

DROP TABLE USER_TASKS;

INSERT INTO USER_TASKS (
    USER_ID,
    TASK_ID
) VALUES (
    "f003",
    "t001"
),
(
    "f003",
    "t002"
),
(
    "f003",
    "t003"
);

INSERT INTO TASKS (
    ID,
    TITLE,
    DESCRIPTION
) VALUES(
    "t001",
    "criar header",
    "criar header para frontend unico labecommerce, labeedit e labebooks"
),
(
    "t002",
    "criar footer",
    "criar footer para frontend unico labecommerce, labeedit e labebooks"
),
(
    "t003",
    "criar componentes do labecommerce",
    "criar componentes para frontend unico labecommerce, labeedit e labebooks"
);

-- Conecte o arquivo pratica-aprofundamento-sql.db com a extensão MySQL e ative a conexão aqui

-- Deletar tabela
DROP TABLE POKEMONS;

-- Criar tabela
CREATE TABLE POKEMONS (
    ID INTEGER PRIMARY KEY UNIQUE NOT NULL,
    NAME TEXT UNIQUE NOT NULL,
    TYPE TEXT NOT NULL,
    HP INTEGER NOT NULL,
    ATTACK INTEGER NOT NULL,
    DEFENSE INTEGER NOT NULL,
    SPECIAL_ATTACK INTEGER NOT NULL,
    SPECIAL_DEFENSE INTEGER NOT NULL,
    SPEED INTEGER NOT NULL
);

-- Popular tabela
INSERT INTO POKEMONS (
    ID,
    NAME,
    TYPE,
    HP,
    ATTACK,
    DEFENSE,
    SPECIAL_ATTACK,
    SPECIAL_DEFENSE,
    SPEED
) VALUES (
    1,
    "bulbasaur",
    "grass",
    45,
    49,
    49,
    65,
    65,
    45
),
(
    2,
    "ivysaur",
    "grass",
    60,
    62,
    63,
    80,
    80,
    60
),
(
    3,
    "venusaur",
    "grass",
    80,
    82,
    83,
    100,
    100,
    80
),
(
    4,
    "charmander",
    "fire",
    39,
    52,
    43,
    60,
    50,
    65
),
(
    5,
    "charmeleon",
    "fire",
    58,
    64,
    58,
    80,
    65,
    80
),
(
    6,
    "charizard",
    "fire",
    78,
    84,
    78,
    109,
    85,
    100
),
(
    7,
    "squirtle",
    "water",
    44,
    48,
    65,
    50,
    64,
    43
),
(
    8,
    "wartortle",
    "water",
    59,
    63,
    80,
    65,
    80,
    58
),
(
    9,
    "blastoise",
    "water",
    79,
    83,
    100,
    85,
    105,
    78
);

-- Buscar todos os pokemons
SELECT
    *
FROM
    POKEMONS;

SELECT
    NAME,
    ATTACK,
    SPECIAL_ATTACK
FROM
    POKEMONS
WHERE
    ATTACK >= 60
    AND SPECIAL_ATTACK >= 60;

-- Práticas

SELECT
    *
FROM
    POKEMONS
WHERE
    ATTACK >= 60;

SELECT
    NAME
FROM
    POKEMONS
WHERE
    NAME LIKE "%saur";

CREATE TABLE CARDS (
    ID TEXT PRIMARY KEY UNIQUE NOT NULL,
    NAME TEXT UNIQUE NOT NULL,
    IMAGE_URL TEXT NOT NULL,
    DESCRIPTION TEXT NOT NULL DEFAULT
);

CREATE TABLE ACCOUNTS(
    ID TEXT PRIMARY KEY UNIQUE NOT NULL,
    OWNER TEXT NOT NULL,
    BALANCE REAL NOT NULL,
    CATEGORY TEXT NOT NULL
);

INSERT INTO ACCOUNTS (
    ID,
    OWNER,
    BALANCE,
    CATEGORY
) VALUES(
    "a001",
    "Ciclano",
    10000,
    "Gold"
),
(
    "a002",
    "Astrodev",
    500000,
    "Black"
),
(
    "a003",
    "Fulana",
    20000,
    "Platinum"
),
(
    "a004",
    "wilsom",
    10500,
    "Ouro"
),
(
    "a0058801bd24-3ce0-42fc-9fd6-be112afdfc35",
    "Melissa",
    10900,
    "Ouro"
);

SELECT
    *
FROM
    ACCOUNTS
WHERE
    OWNER LIKE "%a%"
    SELECT
        NAME,
        DEFENSE
    FROM
        POKEMONS
    ORDER BY
        DEFENSE DESC;

SELECT
    *
FROM
    POKEMONS
GROUP BY
    TYPE;

SELECT
    *
FROM
    ACCOUNTS LIMIT 3;

CREATE TABLE CLASSROOMS(
    ID TEXT PRIMARY KEY UNIQUE NOT NULL,
    NAME TEXT UNIQUE NOT NULL
);

INSERT INTO CLASSROOMS (
    ID,
    NAME
) VALUES (
    'c001',
    'A'
);

SELECT
    *
FROM
    CLASSROOMS;

CREATE TABLE STUDENTS(
    ID TEXT PRIMARY KEY UNIQUE NOT NULL,
    USER_ID TEXT NOT NULL,
    CLASSROOM_ID TEXT NOT NULL,
    FOREIGN KEY (CLASSROOM_ID) REFERENCES CLASSROOM(ID) FOREIGN KEY (USER_ID) REFERENCES USERS(ID)
);

INSERT INTO STUDENTS(
    ID,
    USER_ID,
    CLASSROOM_ID
) VALUES (
    's001',
    'f001',
    'c001'
);

DROP TABLE STUDENTS;

SELECT
    *
FROM
    STUDENTS;

SELECT
    *
FROM
    STUDENTS
    INNER JOIN USERS
    ON USER_ID = USERS.ID;

SELECT
    *
FROM
    USERS
    RIGHT JOIN STUDENTS
    ON USERS.ID = STUDENTS.USER_ID;

CREATE TABLE PHONES(
    ID TEXT PRIMARY KEY UNIQUE NOT NULL,
    USER_ID TEXT NOT NULL,
    PHONE_NUMBER TEXT NOT NULL,
    FOREIGN KEY (USER_ID) REFERENCES USERS(ID)
);

SELECT
    *
FROM
    PHONES
WHERE
    USER.ID = "u001";

INSERT INTO PHONES (
    ID,
    USER_ID,
    PHONE_NUMBER
) VALUES (
    "p001",
    "u001",
    "559399999393"
),
(
    'p002',
    'u001',
    '559399991561'
);

INSERT INTO PHONES (
    ID,
    USER_ID,
    PHONE_NUMBER
) VALUES (
    "p004",
    "u003",
    "5593865135131"
);

SELECT
    *
FROM
    PHONES;

DROP TABLE PHONES;

INSERT INTO PHONES (
    ID,
    USER_ID,
    PHONE_NUMBER
)VALUES (
    "p003",
    "u001",
    "5593865131"
);

SELECT
    EMAIL,
    NAME
FROM
    USERS
    INNER JOIN PHONES
    ON PHONES.USER_ID = USERS.ID;

CREATE TABLE PRODUCTS_PURCHASES(
    PURCHASES_ID TEXT NOT NULL,
    PRODUCT_ID TEXT NOT NULL,
    QUANTITY INTEGER NOT NULL
);

DROP TABLE PRODUCTS_PURCHASES;

INSERT INTO PRODUCTS_PURCHASES(
    PURCHASES_ID,
    PRODUCT_ID,
    QUANTITY
)VALUES(
    "PG001",
    "DFE-3388",
    7
);

CREATE TABLE PURCHASES(
    ID TEXT PRIMARY KEY NOT NULL UNIQUE,
    BUYER TEXT NOT NULL,
    TOTAL_PRICE REAL NOT NULL,
    PAID INTEGER NOT NULL DEFAULT 0,
    CREATED_AT DATETIME DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (BUYER) REFERENCES USERS(ID) ON UPDATE CASCADE ON DELETE CASCADE
);

DROP TABLE PURCHASES;

SELECT
    *
FROM
    PURCHASES;

INSERT INTO PURCHASES(
    ID,
    BUYER,
    TOTAL_PRICE,
    PAID
) VALUES (
    "PG001",
    "400.032.000-21",
    10500,
    1
),
(
    "PG002",
    "268.809.688-56",
    2700,
    1
);

SELECT
    PURCHASES.ID,
    PURCHASES.BUYER,
    PURCHASES.CREATED_AT,
    PURCHASES.TOTAL_PRICE,
    NAME,
    EMAIL
FROM
    USERS
    INNER JOIN PURCHASES
    ON BUYER = USERS.ID;

UPDATE PURCHASES
SET
    TOTAL_PRICE = 5400
WHERE
    ID = "PG002";

SELECT
    *
FROM
    PRODUCTS_PURCHASES;

SELECT
    PRODUCTS.NAME,
    *
FROM
    PURCHASES
    INNER JOIN PRODUCTS_PURCHASES
    ON PURCHASES.ID = PRODUCTS_PURCHASES.PURCHASE_ID
    INNER JOIN PRODUCTS
    ON PRODUCTS_PURCHASES.PRODUCT_ID = PRODUCTS.ID
WHERE
    PURCHASE_ID="PG001";

-- SQLBook: Code
SELECT
    *
FROM
    PRODUCTS_PURCHASES
    INNER JOIN PRODUCTS
    ON PRODUCTS_PURCHASES.PRODUCT_ID = PRODUCTS.ID
    INNER JOIN PURCHASES
    ON PRODUCTS_PURCHASES.PURCHASES_ID = "PG001"
 --pratica relacoes 2
    CREATE TABLE FOLLOWS( FOLLOWER_ID TEXT NOT NULL,
    FOLLOWED_ID TEXT NOT NULL,
    FOREIGN KEY (FOLLOWER_ID) REFERENCES USERS(ID),
    FOREIGN KEY (FOLLOWED_ID) REFERENCES USERS(ID) );

SELECT
    *
FROM
    ACCOUNTS;

SELECT
    *
FROM
    FOLLOWS;

--A SEGUE B E C
-- B SEGUE A
-- C NÃP SEGUE NINGUEM

INSERT INTO FOLLOWS (
    FOLLOWER_ID,
    FOLLOWED_ID
) VALUES (
    "f001",
    "f002"
),
(
    "f001",
    "f003"
),
(
    "f002",
    "f001"
);

SELECT
    *
FROM
    USERS
    INNER JOIN FOLLOWS
    ON FOLLOWS.FOLLOWER_ID = USERS.ID;

SELECT
    *
FROM
    USERS
    LEFT JOIN FOLLOWS
    ON FOLLOWS.FOLLOWED_ID = USERS.ID;

--- PRATICA 3.2

--REMOVER AMBIGUIDADES
SELECT
    USERS.ID AS USERSID,
    USERS.NAME,
    USERS.EMAIL,
    USERS.PASSWORD,
    USERS.CREATED_AT AS CREATEDAT,
    FOLLOWS.FOLLOWER_ID AS FOLLOWERID,
    FOLLOWS.FOLLOWED_ID AS FOLLOWEDID,
    USERFOLLOWED.NAME AS NAMEFOLLOWED
FROM
    USERS
    INNER JOIN FOLLOWS
    ON FOLLOWS.FOLLOWED_ID = USERS.ID
    INNER JOIN USERS AS USERFOLLOWED
    INNER JOIN USERS AS FOLLOWEDID
    ON FOLLOWS.FOLLOWED_ID = USERS.ID;

--RELACOES SQL2 PROJETO


CREATE TABLE PURCHASES_PRODUCTS (
    PURCHASE_ID TEXT NOT NULL,
    PRODUCT_ID TEXT NOT NULL,
    QUANTITY INTEGER NOT NULL,
    FOREIGN KEY (PURCHASE_ID) REFERENCES PURCHASES(ID) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (PRODUCT_ID) REFERENCES PRODUCTS(ID) ON UPDATE CASCADE ON DELETE CASCADE
)
    SELECT
        *
    FROM
        PURCHASES_PRODUCTS;

CREATE TABLE BANDS (
    ID TEXT PRIMARY KEY UNIQUE NOT NULL,
    NAME TEXT NOT NULL
);

CREATE TABLE SONGS (
    ID TEXT PRIMARY KEY UNIQUE NOT NULL,
    NAME TEXT NOT NULL,
    BAND_ID TEXT NOT NULL,
    FOREIGN KEY (BAND_ID) REFERENCES BANDS (ID) ON DELETE CASCADE ON UPDATE CASCADE
);

DROP TABLE SONGS;

DROP TABLE BANDS;

-- Bandas já foram inseridas
INSERT INTO BANDS (
    ID,
    NAME
) VALUES (
    'b001',
    'Evanescence'
),
(
    'b002',
    'LS Jack'
),
(
    'b003',
    'Blink-182'
);

SELECT
    *
FROM
    BANDS;

-- Músicas já foram inseridas
INSERT INTO SONGS (
    ID,
    NAME,
    BAND_ID
) VALUES (
    's001',
    'Bring me to life',
    'b001'
),
(
    's002',
    'Carla',
    'b002'
),
(
    's003',
    'Uma carta',
    'b002'
),
(
    's004',
    'All the small things',
    'b003'
),
(
    's005',
    'I miss you',
    'b003'
);

SELECT
    *
FROM
    SONGS;

-- CONSULTA PARA BANDAS E "USERS" COM INFO DETAIL DE BANDS EM CONSULTA UNICA
SELECT
    *
FROM
    BANDS
    INNER JOIN USERS
    ON BANDS.ID = USERS.ID
WHERE
    BANDS.NAME LIKE "TAN BIONICA";

ALTER TABLE USERS ADD COLUMN AVATAR_IMG TEXT NOT NULL DEFAULT "https://i.postimg.cc/ZYx00WwP/img-Avatar.webp";

ALTER TABLE USERS ADD COLUMN ROLE TEXT NOT NULL DEFAULT "Normal";

SELECT
    *
FROM
    USERS;

SELECT
    *
FROM
    PURCHASES;

SELECT
    *
FROM
    PRODUCTS_PURCHASES;

DROP TABLE ACCOUNTS;

CREATE TABLE ACCOUNTS (
    ID TEXT PRIMARY KEY UNIQUE NOT NULL,
    OWNER TEXT NOT NULL,
    BALANCE REAL DEFAULT (0) NOT NULL,
    CREATED_AT TEXT DEFAULT (DATETIME()) NOT NULL,
    UPDATE_AT TEXT NOT NULL DEFAULT ( DATETIME()),
    FOREIGN KEY (OWNER) REFERENCES USERS (ID) ON UPDATE CASCADE ON DELETE CASCADE
);

INSERT INTO ACCOUNTS (
    ID,
    OWNER_ID
) VALUES (
    'a001',
    'u001'
),
(
    'a002',
    'u002'
);

SELECT
    *
FROM
    PRODUCTS;

-- SQLBook: Markup



SELECT
    *
FROM
    USERS
    INNER JOIN BANDS
    ON BANDS.NAME = USERS.NAME
WHERE
    USERS.ROLE = "Bands";

SELECT
    *
FROM
    PURCHASES;

SELECT
    *
FROM
    PURCHASES_PRODUCTS;

ON ID = PURCHASE_ID;

INSERT INTO PURCHASES VALUES(
    "PG001",
    "u001",
    16500,
    0,
) INSERT INTO PURCHASES_PRODUCTS VALUES(
    "PG001",
    "u001",
    6
)
    SELECT
        *
    FROM
        BANDS
        INNER JOIN USERS
        ON BANDS.ID = USERS.ID;

SELECT
    *
FROM
    PURCHASES;

CREATE TABLE PURCHASES (
    ID TEXT PRIMARY KEY NOT NULL,
    BUYER TEXT NOT NULL,
    FINALPRICE REAL NOT NULL,
    DISCOUNTAPPLY REAL NOT NULL,
    QUANTITYOFPAYMENTS INTEGER NOT NULL,
    CREATED_AT DATETIME DEFAULT '(DATETIME(NOW))',
    ACCOUNT TEXT NOT NULL,
    FOREIGN KEY (BUYER) REFERENCES USERS(ID),
    FOREIGN KEY (ACCOUNT) REFERENCES ACCOUNTS(ID) ON DELETE CASCADE ON UPDATE CASCADE
);

SELECT
    *
FROM
    ACCOUNTS;

--currentBalance
--created_at

DROP TABLE PRODUCTS_PURCHASES;

CREATE TABLE PRODUCTS_PURCHASES(
    PRODUCT_ID TEXT NOT NULL,
    PURCHASE_ID TEXT NOT NULL,
    ACCOUNT_ID TEXT NOT NULL,
    QUANTITY INTEGER,
    START_DATE DATETIME NOT NULL,
    END_DATE DATETIME NOT NULL,
    FOREIGN KEY (PRODUCT_ID) REFERENCES PRODUCTS(ID),
    FOREIGN KEY (PURCHASE_ID) REFERENCES PURCHASES(ID),
    FOREIGN KEY (ACCOUNT_ID) REFERENCES ACCOUNTS(ID)
);

SELECT
    *
FROM
    PURCHASES
    INNER JOIN PRODUCTS_PURCHASES
    ON PRODUCTS_PURCHASES.PURCHASES_ID = PURCHASES.ID;

CREATE TABLE CARDS (
    ID TEXT NOT NULL,
    ID TEXT PRIMARY KEY NOT NULL,
    NAME TEXT NOT NULL,
    FABRICANT TEXT NOT NULL,
    RELEASE_YEAR INTEGER NOT NULL,
    HORSEPOWER INTEGER NOT NULL,
    vMAX INTEGER NOT NULL,
    price REAL NOT NULL,
    image TEXT NOT NULL
)

SELECT * FROM cards;

DROP TABLE cards;
DROP TABLE purchases;
CREATE TABLE purchases (
    id TEXT PRIMARY KEY NOT NULL UNIQUE,
    buyer TEXT NOT NULL,
    total_price REAL NOT NULL,
    created_at TEXT NOT NULL,
    paid INTEGER NOT NULL DEFAULT 0,
    FOREIGN KEY (buyer) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE
);

DROP TABLE PRODUCTS_PURCHASES;
CREATE TABLE products_purchases(
    purchases_id TEXT NOT NULL,
    product_id TEXT NOT NULL,
    quantity INTEGER NOT NULL,
    FOREIGN KEY (purchases_id) REFERENCES purchases(id) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (product_id) REFERENCES products(id) ON UPDATE CASCADE ON DELETE CASCADE
)

SELECT * FROM PRODUCTS_PURCHASES;
SELECT * FROM PURCHASES;
SELECT * FROM users;