import { BaseDatabase } from "./BaseDatabase";
import { TAccount, TAccountDB, TProductDB, TUserDB, USER_ACCOUNT } from "../types/types";
import { accounts } from '../dataTS/accounts';
// CRIACAO DO BASEDATABASE  serve para EXTRAIR A LOGICA ASSOCIADA A EXTRACAO DE INFO DO BANCO DE DADOS, A PARTE QUE FAZ A REQUISICAO DA INFO NAO PRECISA SABER COMO A LOGICA E EXECUTADA

export class AccountsDatabase extends BaseDatabase {
  public static TABLE_ACCOUNTS = "accounts";
  public async findAccount(q: string | undefined) {
    let accountsDB;
    if (!q) {
      const accountsDB: TAccount[] = await BaseDatabase.connection(
        AccountsDatabase.TABLE_ACCOUNTS
      );
      const result = accountsDB;
      return result;
    } else {
      const accountsDB: TAccount[] = await BaseDatabase.connection(
        AccountsDatabase.TABLE_ACCOUNTS
      ).where("owner", "LIKE", `${q}`);

      const result = accountsDB;
      return result;
    }
  }

  public async findAccountById(id: string) {
    const accountsDB = await BaseDatabase.connection(
      AccountsDatabase.TABLE_ACCOUNTS
    ).where("id", "like", `${id}`);

    const result = accountsDB;
    return result;
  }

  public async account4Insert(account4Insert: TAccountDB) {
    await BaseDatabase.connection(AccountsDatabase.TABLE_ACCOUNTS).insert(
      account4Insert
    );
  }

  public async updateAccount(account4update: TProductDB, id:string) {
await BaseDatabase.connection(AccountsDatabase.TABLE_ACCOUNTS).update(
  account4update).
  where("id", id);
  }
}
/*

  public async deleteProduct(id4Delete: string) {
    await BaseDatabase.connection(ProductsDatabase.TABLE_PRODUCTS)
      .delete()
      .where("id", "LIKE", `${id4Delete}`);
  }
  public async findProductsByCategory(category: string) {
    const result = await BaseDatabase.connection(
      ProductsDatabase.TABLE_PRODUCTS
    ).where("category", "like", `%${category}%`);
    return result;
  }*/

