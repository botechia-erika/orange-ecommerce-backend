import { BaseDatabase } from "./BaseDatabase";
import { TProductDB, TUserDB, USER_ACCOUNT } from "../types/types";
import { ProductsPurchases } from "../models/ProductsPurchases";
import { IProductPurchaseDB } from "../interfaces/interfaces";
// CRIACAO DO BASEDATABASE  serve para EXTRAIR A LOGICA ASSOCIADA A EXTRACAO DE INFO DO BANCO DE DADOS, A PARTE QUE FAZ A REQUISICAO DA INFO NAO PRECISA SABER COMO A LOGICA E EXECUTADA

export class ProductsPurchasesDatabase extends BaseDatabase {
  public static TABLE_PRODUCTS_PURCHASES = "products_purchases";
  public async findPurchases(q: string | undefined) {
    let result: IProductPurchaseDB[];
    if (!q) {
      const productPurchaseDB: IProductPurchaseDB[] = await BaseDatabase.connection(
        ProductsPurchasesDatabase.TABLE_PRODUCTS_PURCHASES
      );
        result = productPurchaseDB;
      return result;
    } else {
      const productPurchaseDB: IProductPurchaseDB[] =
        await BaseDatabase.connection(
          ProductsPurchasesDatabase.TABLE_PRODUCTS_PURCHASES
        ).where("buyer", "LIKE", `${q}`);

      result = productPurchaseDB;
      return result;
    }
    return result;
  }

  public async findPurchaseById(id: string) {
    const productDB: IProductPurchaseDB[] = await BaseDatabase.connection(
      ProductsPurchasesDatabase.TABLE_PRODUCTS_PURCHASES
    ).where("id", "like", `${id}`);

    const result = productDB;
    return result;
  }

  public async insertProductPurchase(
    productsPurchase4Insert: IProductPurchaseDB
  ) {await BaseDatabase.connection(
      ProductsPurchasesDatabase.TABLE_PRODUCTS_PURCHASES
    ).insert(productsPurchase4Insert);
  }

  public async updatePurchase(productsPurchases4Edit: ProductsPurchases) {
    const result: IProductPurchaseDB[] = await BaseDatabase.connection(
      ProductsPurchasesDatabase.TABLE_PRODUCTS_PURCHASES
    ).where("id", "LIKE", `${productsPurchases4Edit.getPurchaseId()}`);
  }

  public async deletePurchase(id4Delete: ProductsPurchases) {
    await BaseDatabase.connection(
      ProductsPurchasesDatabase.TABLE_PRODUCTS_PURCHASES
    )
      .delete()
      .where("id", "LIKE", `${id4Delete.getPurchaseId()}`);
  }
}
