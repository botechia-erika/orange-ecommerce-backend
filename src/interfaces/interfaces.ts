//ORANGE PODE CRIAR POSTS  DE PERGUNTAS E AVALIACOES COMPRAR DAR LIKES E DISLIKES 

interface NormalLogged {
    email: string,
    role: "NORMAL"|"BUYER"|"STUDENT",
    password: string,
    permission: "orange",
    isAlive: true
    showLogged: (value1:string)=>{}
}

interface EntepriseLogged {
    email: string,
    role: "ADMIN"|"TEACHER"|"MANAGER",
    password: string,
    permission: "orangered",
    isAlive: true,

}

interface instructorLogged {
    email: string
    role: "INSTRUCTOR",
    password: string,
    permission: "red",
    isAlive: false   
}

export interface IPurchaseDB {
    id: string,
    buyer: string,
    total_price: number,
    created_at: string,
    paid: number
}

export interface IProductPurchaseDB  {
    purchases_id :string,
    product_id :string,
    quantity :number,
}