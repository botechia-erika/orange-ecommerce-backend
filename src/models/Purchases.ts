import { purchaseTicket } from "../types/types";

export class Purchases {
  private id: string;
  private buyer: string;
  private total_price: number;
  private created_at: string;
  private paid: number;

  constructor(
    id: string,
    buyer: string,
    total_price: number,
    created_at: string,
    paid: number
  ) {
    id =  this.id;
    buyer = this.buyer;
    total_price = this.total_price;
    created_at = this.created_at;
    paid = this.paid;
  }

  public getId(): string {
    return this.id;
  }

  public getBuyer(): string {
    return this.buyer;
  }

  public getTotalPrice(): number {
    return this.total_price;
  }

  public getPaid(): number {
    return this.paid;
  }

  public setTotalPrice(value: number): void {
    this.total_price = value;
  }

  public setPaid(value: number): void {
    this.paid = value;
  }


}