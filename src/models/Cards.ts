import { Product } from "./Product";
//https://www.youtube.com/watch?v=P3mDEo0gB7A

interface ICards {
  id: string;
  name: string;
  fabricant: string;
  release_year: number;
  horsePower: number;
  vMAX: number;
  price :number;
  image: string;
}

export class Cards implements ICards {
   id: string;
   name: string;
 fabricant: string;
   release_year: number;
   horsePower: number;
   vMAX: number; 
   price: number; 
   image: string;

  constructor(
    id: string,
    name: string,
    fabricant: string,
    release_year: number,
    horsePower: number,
    vMAX: number,
    price: number,
    image: string
  ) {}

  public getId(): string {
    return this.id;
  }

  public getName(): string {
    return this.name;
  }

  public getFabricant(): string {
    return this.fabricant;
  }

  public getReleaseYear(): number {
    return this.release_year;
  }

  public getHorsePower(): number {
    return this.horsePower;
  }

  public getVMAX(): number {
    return this.vMAX;
  }
  public getPrice(): number {
    return this.price;
  }

  public getImage(): string {
    return this.image;
  }
}


const cardHorse = (card1:Cards , card2:Cards)=>{
  if(card1.getHorsePower() > card2.getHorsePower() ){
    return 1
  }else {
    return 2
  }
}

const cardSpeed = (card1:Cards , card2:Cards)=>{
  if (card1.getVMAX() > card2.getVMAX()) {
    return 3;
  } else {
    return 0;
  }
}

const cardPrice = (card1:Cards , card2:Cards)=>{
  if(card1.getPrice() > card2.getPrice() ){
    return 3
  }else{
    return 0
  }
}

const playGame = (card1:Cards , card2:Cards, select: string):number=>{
 if(select === "horse"){
 return  cardHorse(card1, card2)

 }
  else if (select === "speed") {
    const result = cardSpeed(card1, card2);
    return result;
  }
   else if (select === "price") {
     const result = cardPrice(card1, card2);
     return result;
   }
}




const card2: Cards = new Cards(
  "ba0eb5c8-53cd-44b5-9188-7f64478c26d5",
  "765T",
  "MCLAREM",
  2020,
  437,
  320,
  5000000,
  "https://www.europeanprestige.co.uk/blobs/stock/225/images/f5b32505-cee1-4b2f-8125-959b9d0d2251/1j6a7845-edit.jpg?width=2000&height=1333"
);

const card1: Cards = new Cards(
  "eacd8404-dfd6-437c-9743-1548b835f6ec",
  "718 Boxster",
  "PORSCHE",
  2020,
  755,
  330,
  1030000,
  "https://m.atcdn.co.uk/a/media/w600/ab01c11fe848431e86881078b4502ae8.jpg"
);



console.log(playGame(card1, card2 , "horse"))