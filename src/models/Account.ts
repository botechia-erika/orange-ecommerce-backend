
export class Account{
  constructor(
    private id: string,
    private balance: number,
    private owner: string,
    private created_at: string,
    private update_at: string
  ) {}

  public getId(): string {
    return this.id;
  }

  public getCreatedAt(): string {
    return this.created_at;
  }

  public getUpdateAt(): string {
    return this.update_at;
  }

  public getBalance(): number {
    return this.balance;
  }

  public setBalance(value: number): void {
    this.balance = value;
  }

  public getOwner(): string {
    return this.owner;
  }

  public setOwnerId(value: string): void {
    this.owner = value;
  }

  public setUpdateAt(value: string): void {
    this.update_at = value;
  }

  calculateBalance(value:number):number |string{
    if(value<this.getBalance()){
      return this.getBalance() + value
    }else if(value > this.getBalance() && this.getBalance() + value <0 ){
      return this.getBalance()
    }else{
      return this.getBalance() + value
    }
  }
}