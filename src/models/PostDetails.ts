export class Product{
    constructor(
     private id:string,
     private idProduct: string,  
     private likes:string,
     private dislikes :string ,
     private likes_dislikes:0|1|2,
     private rating: 0,
     private feedBackList : ["Lista Vazia"]
     ){}

       public getId():string{
        return this.id
       }


       public getName():string{
        return this.name
       }

       public getDescription():string{
        return this.description
       }

       public setDescription (value:string):void{
            this.description = value
       }
       public getImageUrl():string{
        return this.imageUrl
       }

       public setImageUrl (value:string):void{
            this.imageUrl = value
       }
       public getPrice():number{
        return this.price
       }

       public setPrice (value:number):void{
            this.price = value
       }
       
       public getCategory():string{
          return this.category
         }
  
         public seCategory(value:string):void{
              this.category = value
         }
}

