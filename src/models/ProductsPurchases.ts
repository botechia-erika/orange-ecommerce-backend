
export class ProductsPurchases {
  private product_id: string;
  private purchases_id: string;
  private quantity: number;
  constructor(
    product_id: string,
    purchases_id: string,
    quantity: number,
  ) {
    this.product_id = product_id
      this.purchases_id = purchases_id
      this.quantity = quantity
  }

  public getProductId(): string {
    return this.product_id;
  }

  public getPurchaseId(): string {
    return this.purchases_id;
  }


  public getQuantity(): number {
    return this.quantity;
  }

  public setQuantity(value: number): void {
    this.quantity = value;
  }
}