export class Game {
  static winner = 3;
  static looser = 0;
  static equal = 1;

  constructor(
    private value1: number,
    private value2: number,
    private value3: number
  ) {}

  public getValue1(): number {
    return this.value1;
  }

  public getValue2(): number {
    return this.value2;
  }

  public getValue3(): number {
    const result = this.getValue1() - this.getValue2()
    if(result>0){
    return this.value3 = 3;
    }else if (result === 0){
        return this.value3 = 1
    }else{
        return this.value3=0
    }
  }
}