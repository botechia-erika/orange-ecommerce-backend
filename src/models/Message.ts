/*
  TEXT MESSAGE
    id: string
    senderId: string
    chatId: string
    content: string
    createdAt: string

    send: () => void da mensagem de texto

  --------------------------------

  VIDEO MESSAGE
    id: string
    senderId: string
    chatId: string
    content: string
    createdAt: string

    video: string

    upload: () => void
    send: () => void da mensagem de vídeo
*/

// public > protected > private

class Message {
  constructor(
    protected id: string,
    protected senderId: string,
    protected chatId: string,
    protected content: string,
    protected createdAt: string
  ) {}
}

class TextMessage extends Message {
  constructor(
    id: string,
    senderId: string,
    chatId: string,
    content: string,
    createdAt: string
  ) {
    super(
      id,
      senderId,
      chatId,
      content,
      createdAt
    )
  }

  public send(): void {
    // envia a msg
    console.log(this.id)
  }
}

class VideoMessage extends Message {
  constructor(
    id: string,
    senderId: string,
    chatId: string,
    content: string,
    createdAt: string,
    private video: string
  ) {
    super(
      id,
      senderId,
      chatId,
      content,
      createdAt
    )
  }

  private upload() {
    // faz o upload do video
  }

  public send() {
    this.upload()
    // envia a msg
  }
}

const textMessage = new TextMessage(
  "t001",
  "u003",
  "c021",
  "ou, me responde!",
  new Date().toLocaleDateString()
)

textMessage.send()

