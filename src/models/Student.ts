export class Student {
  public static role:"Student"= "Student";

  constructor(
    private id: string,
    private name: string
  )
  {}

  public getId(): string {
    return this.id;
  }

  public getName(): string {
    return this.name;
  }

  public setName(value: string): void {
    this.name = value;
  }

}
