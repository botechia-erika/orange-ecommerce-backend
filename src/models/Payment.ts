export class Payment {
  constructor(
    private id: string,
    private buyer: string, 
    private finalPrice: number,
    private discountApply: number,
    private quantityOfPayments: number,
    private parcialPaymentValue: number,
    private currentBalance: number
  ) {}

  // Getters
  get getId(): string {
    return this.id;
  }

  get getBuyer(): string {
    return this.buyer;
  }

  get getFinalPrice(): number {
    return this.finalPrice;
  }

  get getDiscountApply(): number {
    return this.discountApply;
  }

  get getQuantityOfPayments(): number {
    return this.quantityOfPayments;
  }

  get getParcialPaymentValue(): number {
    return this.parcialPaymentValue;
  }

  get getCurrentBalance(): number {
    return this.currentBalance;
  }

  // Setters
  set setId(id: string) {
    this.id = id;
  }

  set setFinalPrice(finalPrice: number) {
    this.finalPrice = finalPrice;
  }

  set setDiscountApply(discountApply: number) {
    this.discountApply = discountApply;
  }

  set setQuantityOfPayments(quantityOfPayments: number) {
    this.quantityOfPayments = quantityOfPayments;
  }

  set setParcialPaymentValue(parcialPaymentValue: number) {
    this.parcialPaymentValue = parcialPaymentValue;
  }

  set setCurrentBalance(currentBalance: number) {
    this.currentBalance = currentBalance;
  }
  // Método para calcular o desconto com base nas regras fornecidas
  calculateDiscount(): number {
    let discount = 0;

    // 20% de desconto - Aplicado para pagamentos à vista ou parcelados em até 2x
    if (this.quantityOfPayments <= 2) {
      discount += this.finalPrice * 0.2;
    }

    // 15% de desconto - Aplicado na compra de 3 cursos
    if (this.quantityOfPayments >= 3) {
      discount += this.finalPrice * 0.15;
    }

    // 10% de desconto - Aplicado na compra de 2 cursos
    if (this.quantityOfPayments === 2) {
      discount += this.finalPrice * 0.1;
    }

    return discount;
  }
   // Método para calcular o preço total com desconto
   calculateTotalPriceWithDiscount(): number {
    const discount = this.calculateDiscount();
    const totalPriceWithDiscount = this.finalPrice - discount;
    return totalPriceWithDiscount;
  }

  // Método para calcular o valor de cada parcela
  calculateInstallmentValue(): number {
    const totalPriceWithDiscount = this.calculateTotalPriceWithDiscount();
    const installmentValue = totalPriceWithDiscount / this.quantityOfPayments;
    return installmentValue;
  }

  generateFinancialReport(): string {
    const totalPriceWithDiscount = this.calculateTotalPriceWithDiscount();
    const installmentValue = this.calculateInstallmentValue();
    const remainingBalance = this.currentBalance - totalPriceWithDiscount;

    const report = `
      Relatório Financeiro do Estudante:
      ----------------------------------
      ID do Pagamento: ${this.id}
      ID do Cliente: ${this.buyer}
      Preço BASE do Rental: R$ ${this.finalPrice}
      Desconto Aplicado: R$ ${this.calculateDiscount()}
      Valor Total com Desconto: R$ ${totalPriceWithDiscount}
      Quantidade de Parcelas: ${this.quantityOfPayments}
      Valor de Cada Parcela: R$ ${installmentValue}
      Saldo Atual: R$ ${this.currentBalance}
      Saldo Restante Após Pagamento: R$ ${remainingBalance}
    `;

    return report;
  }
}
