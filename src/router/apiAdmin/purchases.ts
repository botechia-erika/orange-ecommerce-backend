
import express,  { Router } from "express";
import  {PurchaseController} from './../../controllers/apiAdmin/purchasesController'
import { purchaseTicket } from '../../types/types';

const purchaseController = new PurchaseController();

const router = express.Router()

router.post("/", purchaseController.createPurchase);
/*router.get("/:id" , purchasesController.getPurchaseById)
router.delete("/:id" , purchasesController.destroyPurchase)
router.post("/", purchasesController.createPurchase)*/

export default router
