import express, {Router} from 'express'
import  {AccountsController} from '../../controllers/apiBank/accountsController';

const accountsController = new AccountsController()
const router = express.Router()

router.get('/', accountsController.getAccounts)
/* router.get('/:id/balance', accountsController.getAccountBalance)
router.post('/' , accountsController.createAccount)

router.put('/:id/balance' , accountsController.editAccountBalance) */

export default router;