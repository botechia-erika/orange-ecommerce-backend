import express,  { Router } from "express";
import { UserController } from '../../controllers/apiUsers/UsersController';


const userController = new UserController();

const router = express.Router()

router.get('/', userController.getUsers)
router.post('/', userController.createUser)
router.put('/:id', userController.editUserById)
router.get('/:id', userController.getUserById)
router.delete("/:id", userController.destroyUser)

export default router