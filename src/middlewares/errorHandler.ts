import { Request, Response, NextFunction } from "express";

export function errorHandler(
  err: Error,
  req: Request,
  res: Response,
  next: NextFunction
): void {
  console.error(err.message);

  const statusCode = res.statusCode === 200 ? 500 : res.statusCode;
  res.status(statusCode);

  if (err instanceof Error) {
    res.json({ message: err.message });
  } else {
    res.json({ message: "Erro inesperado" });
  }
}
