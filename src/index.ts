process.env.NODE_ENV = process.env.NODE_ENV || "development";
process.env.APP_ENV = process.env.APP_ENV || "development";
import dotenv from "dotenv";


console.log(process.env.APP_FOO);
import express from "express";
import { Request, Response } from "express";
import cors from "cors";
import bodyParser from "body-parser";
import morgan from "morgan";
import { sendHello } from "./helpers/sendHello";
import usersRouter from "./router/apiUsers/users";
import purchasesRouter from './router/apiAdmin/purchases'
import productsRouter from "./router/apiProducts/products";

//import { Purchases, ProductPurchased } from './models/Purchases';

import accountsRouter from "./router/apiBank/accounts";
const app = express();
app.use(morgan("dev"));
app.use(cors());

//app.use(express.json())
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
//app.use(express.static(path.resolve(__dirname, "./../public/")))
app.get("/ping", (req: Request, res: Response) => {
  res.send("Pong");
});


//app.use("/accounts", accountsRouter);
app.use("/users", usersRouter);
app.use("/products", productsRouter);
app.use("/purchases", purchasesRouter);
app.get("/", (req: Request, res: Response) => {
  res.send(
    `${sendHello("Hello ORANGE Word")}`);
});
const port = process.env.PORT ||"3003"

app.listen(port, () => {
  console.log(`Servidor rodando na porta ${port}`)
});


