import { Request, Response } from "express";
import { Account } from '../../models/Account';
import { ACCOUNT_TYPE, TAccountDB, TUserDB, TAccount, TAccountDBPost } from '../../types/types';
import { UserDataBase } from "../../database/UserDatabase";
import { createId } from "../../helpers/createId";
import fs from "fs";
import path from "path";
import { BaseDatabase } from "../../database/BaseDatabase";
import { AccountsDatabase } from '../../database/AccountsDatabase';
import { accounts } from '../../dataTS/accounts';

const pathScore = path.join(__dirname, "./../../../dataJSON/dataScore.json");
const scoreData = JSON.parse(fs.readFileSync(pathScore, "utf-8"));

export class AccountsController extends BaseDatabase {
  public getAccounts = async (req: Request, res: Response) => {
    try {
      const q = req.query.q as string | undefined;
      let accountsDB;
      if (q) {
        accountsDB = await BaseDatabase.connection("accounts").where(
          "owner",
          "LIKE",
          `%${q}%`
        );

        if (![accountsDB]) {
          res.status(404);
          throw new Error("404 ACCOUNT NOT FOUND");
        }
      } else {
        accountsDB = await BaseDatabase.connection("accounts");
      }

      res.status(200).json(accountsDB);
    } catch (error) {
      console.error(error);
      res
        .status(500)
        .send(error instanceof Error ? error.message : "Erro inesperado");
    }
  };
  public getAccountBalance = async (req: Request, res: Response) => {
    try {
      const id = req.params.id;
      const today = new Date().toISOString();
      const [accountsDB]: TAccountDB[] | undefined[] =
        await BaseDatabase.connection("accounts").where(
          "id",
          "LIKE",
          `%${id}%`
        );

      if (!accountsDB) {
        res.status(404).send("'id' não encontrado");
        return;
      }

      const account = new Account(
        accountsDB.id,
        accountsDB.balance,
        accountsDB.owner,
        accountsDB.created_at,
        today
      );

      const result = account.getBalance();
      res.status(200).send({ result });
    } catch (error) {
      console.error(error);
      res
        .status(500)
        .send(error instanceof Error ? error.message : "Erro inesperado");
    }
  };

  public createAccount = async (req: Request, res: Response) => {
    try {
      const newId = req.body.inputId as string | undefined;
      const ownerId = req.body.inputOwner as string;
      const today = new Date().toISOString();
      const idCreated = createId(newId);

      if (typeof ownerId !== "string") {
        res.status(400).send("'ownerId' deve ser uma string");
        return;
      }

      const accountsDatabase = new AccountsDatabase()

      if(newId){
       const idSelect =  accountsDatabase.findAccountById(newId)
       if(idSelect){
          res.status(400).send("'id' já existe");
          return;
        }
      }




      const newAccount = new Account(
        idCreated,
        0,
        ownerId,
        today,
        today
      );

      const account4Insert: TAccountDB = {
        id: newAccount.getId(),
        owner: newAccount.getOwner(),
        balance: newAccount.getBalance(),
        created_at: newAccount.getCreatedAt(),
        update_at: newAccount.getUpdateAt(),
      };

      accountsDatabase.account4Insert(account4Insert)

      res.status(201).send("Conta criada com sucesso");
    } catch (error) {
      console.error(error);
      res
        .status(500)
        .send(error instanceof Error ? error.message : "Erro inesperado");
    }
  };
public editAccountBalance = async (req: Request, res: Response) => {
    try {
      const id = req.params.id as string
      const inputBalance = req.body.inputBalance;

      if (typeof inputBalance !== "number") {
        res.status(400).send("'inputBalance' deve ser um número");
        return;
      }
      const accountsDatabase = new AccountsDatabase()
const accountDB = await accountsDatabase.findAccountById(id)
          if (!accountDB) 
          {
            res.status(404)
            throw new Error("'id' não encontrado");
          }

      const updateBalance = (
        currentBalance: number,
        inputBalance: number
      ): number => {
        if (currentBalance >= -inputBalance) {
          return currentBalance + inputBalance;
        } else {
          res.status(400).send("Saldo insuficiente, operação inválida");
          throw new Error("Saldo insuficiente, operação inválida");
        }
      };

      const defineAccountCategory = (newBalance: number): string => {
        if (newBalance < 5000) {
          return "Bronze";
        } else if (newBalance < 10000) {
          return "Silver";
        } else if (newBalance < 15000) {
          return "Gold";
        } else if (newBalance < 20000) {
          return "Black";
        } else {
          return "Platinum";
        }
      };

      const today = new Date().toISOString();

      const newBalance = updateBalance(accountDB.balance, inputBalance);
      const newCategory = defineAccountCategory(newBalance);

      const account = new Account(
        accountDB.id,
        newBalance,
        accountDB.owner,
        accountDB.created_at,
        today
      );

      await BaseDatabase.connection("accounts")
        .update({ balance: newBalance })
        .where("id", id);

      res.status(200).send(account);
    } catch (error) {
      console.error(error);
      res
        .status(500)
        .send(error instanceof Error ? error.message : "Erro inesperado");
    }
  };
}
  /*

  public createAccount = async (req: Request, res: Response) => {
    try {
      const newId = req.body.inputId as string | undefined;
      const ownerId = req.body.inputOwner as string;
      const today = new Date().toISOString();
      const idCreated = createId(newId);

      if (typeof ownerId !== "string") {
        res.status(400).send("'ownerId' deve ser uma string");
        return;
      }

      if (newId) {
        const [accountDBExists]: TAccountDB[] | undefined[] =
          await BaseDatabase.connection("accounts").where(
            "id",
            "LIKE",
            `%${newId}%`
          );

        if (accountDBExists) {
          res.status(400).send("'id' já existe");
          return;
        }
      }

      if (!ownerId) {
        res.status(400).send("Usuario não informado");
        return;
      }

      const userDataBase = new UserDataBase();
      const userExists = userDataBase.findUserById(ownerId);

      if (!userExists) {
        res.status(404).send("Usuário não cadastrado");
        return;
      }

      const newAccount = new Account(
        idCreated,
        0,
        ownerId,
        ACCOUNT_TYPE.BRONZE,
        today,
        today
      );

      const newAccountDB = {
        id: newAccount.getId(),
        balance: newAccount.getBalance(),
        owner_id: newAccount.getOwner(),
        created_at: newAccount.getCreatedAt(),
        update_at: newAccount.getUpdateAt(),
      };

      await BaseDatabase.connection("accounts").insert(newAccountDB);
      res.status(201).send("Conta criada com sucesso");
    } catch (error) {
      console.error(error);
      res
        .status(500)
        .send(error instanceof Error ? error.message : "Erro inesperado");
    }
  };

  public editAccountBalance = async (req: Request, res: Response) => {
    try {
      const id = req.params.id;
      const inputBalance = req.body.inputBalance;

      if (typeof inputBalance !== "number") {
        res.status(400).send("'inputBalance' deve ser um número");
        return;
      }

      const [accountDB]: TAccountDB[] | undefined[] =
        await BaseDatabase.connection("accounts").where("id", id);

      if (!accountDB) {
        res.status(404).send("'id' não encontrado");
        return;
      }

      const updateBalance = (
        currentBalance: number,
        inputBalance: number
      ): number => {
        if (currentBalance >= -inputBalance) {
          return currentBalance + inputBalance;
        } else {
          res.status(400).send("Saldo insuficiente, operação inválida");
          throw new Error("Saldo insuficiente, operação inválida");
        }
      };

      const defineAccountCategory = (newBalance: number): string => {
        if (newBalance < 5000) {
          return "Bronze";
        } else if (newBalance < 10000) {
          return "Silver";
        } else if (newBalance < 15000) {
          return "Gold";
        } else if (newBalance < 20000) {
          return "Black";
        } else {
          return "Platinum";
        }
      };

      const today = new Date().toISOString();

      const newBalance = updateBalance(accountDB.balance, inputBalance);
      const newCategory = defineAccountCategory(newBalance);

      const account = new Account(
        accountDB.id,
        newBalance,
        accountDB.owner_id,
        newCategory,
        accountDB.created_at,
        today,
        today
      );

      await BaseDatabase.connection("accounts")
        .update({ balance: newBalance })
        .where("id", id);

      res.status(200).send(account);
    } catch (error) {
      console.error(error);
      res
        .status(500)
        .send(error instanceof Error ? error.message : "Erro inesperado");
    }
  };
}
*/
