import { Request, Response } from "express";
import { createId } from "../../helpers/createId";
import { User } from "../../models/User";
import { ROLE, TUserDB } from '../../types/types';
import { UserDataBase } from '../../database/UserDatabase';
import { Product } from "../../models/Product";
import { matchDescriptionCategory } from "../../helpers/matchDescriptionCategory";
import { ProductsDatabase } from '../../database/ProductDatabase';
import { Purchases } from "../../models/Purchases";

import { ProductsPurchases } from '../../models/ProductsPurchases';
import { ProductsPurchasesDatabase } from "../../database/ProductsPurchasesDatabase";
import { PurchasesDataBase } from "../../database/PurchasesDatabase";
import { IProductPurchaseDB, IPurchaseDB } from "../../interfaces/interfaces";


export class PurchaseController {
  /* -----------GET USERS ------------------- */
  TABLE_PRODUCTS = "products";
  TABLE_USERS = "users";
  TABLE_PURCHASES = "purchases";
  public createPurchase = (async (req: Request, res: Response) => {
    try {
      // requerimento de dados do front

      const newId = "undefined";

      const buyerId = req.body.buyerId as string;
      const buyerName = req.body.buyerName as string;
      const buyerUsername = req.body.buyerUsername as string;
      const buyerPassword = req.body.buyerPassword as string;
      const buyerEmail = req.body.buyerEmail as string;
      const buyerRole = req.body.buyerRole as ROLE;
      const buyerAvatar = req.body.buyerAvatar as string;
      const productId = req.body.productId as string;
      const productName = req.body.productName as string;
      const productImg = req.body.productImg as string;
      const productDescription = req.body.productDescription as string;
      const purchaseCreatedAt = req.body.purchaseCreatedAt as string;
      const purchasePaid = req.body.purchasePaid as number;
      const productPrice = req.body.productPrice as number;
      const purchaseProductQuantity = req.body
        .purchaseProductQuantity as number;
      const purchaseFinalPrice = req.body.purchaseFinalPrice as number;

      const userData: User = new User(
        buyerId,
        buyerName,
        buyerUsername,
        buyerPassword,
        buyerEmail,
        buyerRole,
        buyerAvatar,
        Date.now().toString()
      );

   const userDataBase = new UserDataBase();

  const checkUser =  userDataBase.findUserById(buyerId);
 
const idUser = userDataBase.findIdUser(buyerId);
 const user4insert: TUserDB = {
      id: buyerId,
      name: buyerName,
      nickname:buyerUsername,
      email: buyerEmail,
      password: buyerPassword,
      created_at: new Date().toISOString(),
      avatar_img: buyerAvatar,
      role: "Buyer"
  }

  if( !idUser || idUser === null ||  (await idUser).length < 1){
    userDataBase.insertUser(user4insert)
  }else{
    userDataBase.updateUser(user4insert)
  }

  const productData: Product = new Product(
    productId,
    productName,
    productDescription,
    productImg,
    productPrice,
    matchDescriptionCategory(productPrice)
  );

  const productsDatabase = new ProductsDatabase();
  await productsDatabase.findProductById(productData.getId());

  const newPurchaseCode = "pgo-" + createId(newId)
  const purchaseTicket: Purchases = new Purchases(
    newPurchaseCode,
    userData.getId(),
    purchaseFinalPrice,
    purchaseCreatedAt,
    purchasePaid
  );
;
  const purchase4insert: IPurchaseDB = {
    buyer: buyerId,
    created_at: new Date().toISOString(),
    id: purchaseTicket.getId(),
    paid: 0,
    total_price: purchaseFinalPrice
  };

  const purchasesDatabase = new PurchasesDataBase();
  await purchasesDatabase.insertProductPurchase(purchase4insert);

  const purchaseProduct4Insert: IProductPurchaseDB = {
    purchases_id: purchase4insert.id,
    product_id: productId,
    quantity: purchaseProductQuantity,
  };
  const productsPurchases = new ProductsPurchasesDatabase();
  await productsPurchases.insertProductPurchase(purchaseProduct4Insert);

  res.status(201).json("pre reserva realizado com sucesso");

    } catch (error) {
console.error(error);

if (req.statusCode === 200) {
  res.status(500);
}

if (error instanceof Error) {
  res.send(error.message);
} else {
  res.send("Erro inesperado");
}
}
})
}