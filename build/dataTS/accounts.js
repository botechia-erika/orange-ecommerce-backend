"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.accounts = void 0;
const types_1 = require("./../types/types");
exports.accounts = [
    {
        id: "a001",
        ownerName: "Ciclano",
        balance: 10000,
        type: types_1.ACCOUNT_TYPE.GOLD
    },
    {
        id: "a002",
        ownerName: "Astrodev",
        balance: 500000,
        type: types_1.ACCOUNT_TYPE.BLACK
    },
    {
        id: "a003",
        ownerName: "Fulana",
        balance: 20000,
        type: types_1.ACCOUNT_TYPE.PLATINUM
    }
];
//# sourceMappingURL=accounts.js.map